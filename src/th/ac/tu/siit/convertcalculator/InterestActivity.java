package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float IntRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);

		Button b1 = (Button) findViewById(R.id.InterestConvert);
		b1.setOnClickListener(this);

		Button b2 = (Button) findViewById(R.id.IntSetting);
		b2.setOnClickListener(this);

		TextView Intrate = (TextView) findViewById(R.id.Intrate);
		IntRate = Float.parseFloat(Intrate.getText().toString());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();

		if (id == R.id.InterestConvert) {

			EditText edtTextDeposit = (EditText) findViewById(R.id.editDeposit);
//			String sP = edtTextDeposit.getText().toString();

			EditText edtTextAmountofyears = (EditText) findViewById(R.id.editYear);

//			String sT = edtTextAmountofyears.getText().toString();

			

			String res = "";
			try {
				double vP = Double.parseDouble(edtTextDeposit.getText().toString());
				double vT = Double.parseDouble(edtTextAmountofyears.getText().toString());

				double A = vP * Math.pow((1 + (IntRate / 100)), vT);
				res = String.format(Locale.getDefault(), "%.2f", A);
			} catch (NumberFormatException e) {
				res = "Invalid input";
			} catch (NullPointerException e) {
				res = "Invalid input";
			}
			
			
			TextView tvTHB = (TextView) findViewById(R.id.Result);

			tvTHB.setText(res);

		}

		else if (id == R.id.IntSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("IntRate", IntRate);
			startActivityForResult(i, 9999);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			IntRate = data.getFloatExtra("interestRate", 10.0f);
			TextView Intrate = (TextView) findViewById(R.id.Intrate);
			Intrate.setText(String.format(Locale.getDefault(), "%.2f", IntRate));
		}
	}

}
